from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'principal.views.index'),
    url(r'^agregar/$', 'principal.views.agregar_producto'),
    url(r'^borrar/(?P<id_producto>\d+)$', 'principal.views.borrar_producto'),
    url(r'^editar/(?P<id_producto>\d+)$', 'principal.views.editar_producto'),
)
